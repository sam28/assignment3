#!/bin/python3
import csv, random, argparse
import torch
import numpy as np
import pandas as pd
import torch.optim as optim
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset,DataLoader
from torch.utils.data import random_split
from gensim.models import KeyedVectors
from torch.utils.data.sampler import SubsetRandomSampler
from torch.utils.data.sampler import SequentialSampler
import string
import re

#NonComplasory Module
#import matplotlib.pyplot as plt
#import seaborn as sns
import time
#sns.set()

class imdb_HAN_dataset(Dataset):
    def __init__(self,path,num_sntnc,num_word,embad_model,embedlen,train_data=True):
        self.data_frame=pd.read_csv(path,)
        self.model=embad_model
        self.sen_per_review=num_sntnc
        self.word_per_sen=num_word
        self.embedlen=embedlen
        self.train_data=train_data
        remove_char='"#$%&\'()*+-/:;<=>@[\\]^_`{|}~'
        self.table = str.maketrans('', '',remove_char)
#         if 'zz' in self.word_count:
#             self.word_count['zz']+=len(self)*self.h-self.total_word
#         else:
#             self.word_count['zz']=len(self)*self.h-self.total_word
#         self.total_word=len(self)*self.h
        
    def __len__(self):
        return len(self.data_frame)
    
    def __getitem__(self, idx):
        if (self.train_data):
            return {'review':self.data_dict[idx]['review'],'rating':self.data_dict[idx]['rating']}
        else:
            return {'review':self.data_dict[idx]['review']}
    def init_vocab(self):
        self.vocab,self.not_invocab,self.word_count,self.total_word = self.get_vacabulary()
    
    def init_vocab_with_dataset(self,data_set):
        self.vocab,self.not_invocab,self.word_count,self.total_word = \
        data_set.vocab,data_set.not_invocab,data_set.word_count,data_set.total_word
    
    def init_data_dict(self):
        self.data_dict=self.build_data_dict_fixed_dim()
        
    def init_data_dict_with_dataset(self,data_set):
        self.data_dict=data_set.data_dict
    
    def init_vocab_embed(self):
        self.vocabmat=torch.rand(self.embedlen,len(self.vocab)+1)
        self.indtoword={}
        self.wordtoind={}
        for i,w in enumerate(self.vocab):
            self.vocabmat[:,i]=torch.tensor(self.model[w]/np.linalg.norm(self.model[w]),device=device)
            self.indtoword[i]=w
            self.wordtoind[w]=i
        self.indtoword[len(self.vocab)]='PADD_NULL'
        self.wordtoind['PADD_NULL']=int(len(self.vocab))
        self.vocabmat[:,len(self.vocab)]=torch.tensor(self.model['za']*0)   
        
    def init_vocab_embed_with_dataset(self,data_set):
        self.vocabmat=data_set.vocabmat
        self.indtoword=data_set.indtoword
        self.wordtoind=data_set.wordtoind
        
    def init_vocab_parameter(self):#order of initializasation matters here
        self.init_vocab()
        self.init_vocab_embed()
        self.init_data_dict()
        
    def init_vocab_parameter_with_dataset(self,data_set):
        self.init_vocab_with_dataset(data_set)
        self.init_vocab_embed_with_dataset(data_set)
        self.init_data_dict_with_dataset(data_set)
        
    def get_vacabulary(self):
        vocab=set()
        not_invocab=set()
        word_count={}
        word_count['za']=0
        total_word=0
        for i in range(len(self)):
            pp1=re.findall('\[.*?\]',self.data_frame.loc[i]['review'],re.I)#regex '\[.*?\]' {? is used for non greedy choice, produce result by consumming minimum charcter}
            for sen in pp1:
                pp2=re.findall('(\".+?\")+',sen)
                for w in pp2:
                    w=w[1:-1]
                    w1=str.lower(w.translate(self.table))
                    if w1 in self.model.vocab:
                        vocab.add(w1)
                        if w1 in word_count:
                            word_count[w1]+=1
                        else:
                            word_count[w1]=1
                    else:
                        not_invocab.add(w1)
                        word_count['za']+=1
                    total_word+=1
        return vocab,not_invocab,word_count,total_word
    
    def build_data_dict(self):
        data_dict={}
        for idx in range(len(self)):
            element={}
            review=[]
            label=0
            pp1=re.findall('\[.*?\]',self.data_frame.loc[idx]['review'],re.I)#regex '\[.*?\]' {? is used for non greedy choice, produce result by consumming minimum charcter}
            for i,sen in enumerate(pp1):
                temp_l=[]
                pp2=re.findall('(\".+?\")+',sen)
                for j,w in enumerate(pp2):
                    w=w[1:-1]
                    w1=str.lower(w.translate(self.table))
                    if w1 in self.model.vocab:
                        temp_l.append(self.wordtoind[w1])
                    else:
                        if(w1=="n't"):
                            temp_l.append(self.wordtoind['not'])
                        elif(w1=="'ve"):
                            temp_l.append(self.wordtoind['have'])
                        elif(w1=="'re"):
                            temp_l.append(self.wordtoind['are'])
                        elif(w1=="wo"):
                            temp_l.append(self.wordtoind['will'])
                        else:
                            temp_l.append(self.wordtoind['za'])
                review.append(temp_l)
            if self.data_frame.loc[idx][1]=='pos':
                label=1
            else:
                label=0
            element['review']=review
            element['rating']=label
            data_dict[idx]=element
        return data_dict
    
    def build_data_dict_fixed_dim(self):
        data_dict={}
        for idx in range(len(self)):
            element={}
            X=torch.zeros(self.sen_per_review,self.word_per_sen,dtype=torch.long) 
            label=0
            pp1=re.findall('\[.*?\]',self.data_frame.loc[idx]['review'],re.I)#regex '\[.*?\]' {? is used for non greedy choice, produce result by consumming minimum charcter}
            for i,sen in enumerate(pp1):
                if i >=self.sen_per_review:
                    break
                pp2=re.findall('(\".+?\")+',sen)
                for j,w in enumerate(pp2):
                    if j >=self.word_per_sen-1:
                        break
                    w=w[1:-1]
                    w1=str.lower(w.translate(self.table))
                    if w1 in self.model.vocab:
                        X[i][j]=self.wordtoind[w1]
                    else:
                        if(w1=="n't"):
                            X[i][j]=self.wordtoind['not']
                        elif(w1=="'ve"):
                            X[i][j]=self.wordtoind['have']
                        elif(w1=="'re"):
                            X[i][j]=self.wordtoind['are']
                        elif(w1=="wo"):
                            X[i][j]=self.wordtoind['will']
                        else:
                            X[i][j]=self.wordtoind['za']
                lj=j
                if(lj<=self.word_per_sen-1):
                    X[i][lj]=self.wordtoind['.']
                    lj+=1
                    while(lj<self.word_per_sen):
                        X[i][lj]=self.wordtoind['PADD_NULL']
                        lj+=1
            li=i
            while(li<self.sen_per_review):
                for j in range(self.word_per_sen):
                    X[li][j]=self.wordtoind['PADD_NULL']
                li+=1
                
            if(self.train_data):
                if self.data_frame.loc[idx][1]=='pos':
                    label=1
                else:
                    label=0
                element['rating']=label
            
            element['review']=X
            data_dict[idx]=element
        return data_dict


def data_stats(df):    
    word_len=[]
    sen_count=[]
    word_count=[]
    for i in range(len(df)):
        pp1=re.findall('\[.*?\]',df.loc[i]['review'],re.I)
        sen_count.append(len(pp1))
        for j in range(len(pp1)):
            pp2=re.findall('(\".+?\")+',pp1[j])
            word_count.append(len(pp2))
            for k in range(len(pp2)):
                word_len.append(len(pp2[k])-2)

    # plt.subplot(211)
    p_value=90
    plt.title("word_per_sentence plot, total sentence="+str(len(word_count)))
    plt.plot(word_count)
    plt.figtext(.35,.75,str(p_value)+"_percentile ="+str(np.percentile(word_count,p_value)))
    plt.show()
    plt.title("sentence_per_review plot, total Review="+str(len(sen_count)))
    # plt.subplot(221)
    plt.plot(sen_count)
    plt.title("sentence_per_review plot, total Review="+str(len(sen_count)))
    plt.figtext(.55,.75, str(p_value)+"percentile ="+str(np.percentile(sen_count,p_value)))
    plt.plot(sen_count)
    plt.show()
    
class HCNN(torch.nn.Module):
    def __init__(self,out_c1,out_c2,k1_max_pool,k2_max_pool,sen_len,kernel_height,kernel_width,classes):
        super(HCNN,self).__init__()
        self.strd=1
        self.out_c1=out_c1
        self.out_c2=out_c2
        self.k1_max_pool=k1_max_pool
        self.k2_max_pool=k2_max_pool
        self.sen_len=sen_len
        self.kernel_height=kernel_height
        self.kernel_width=kernel_width
        self.classes=classes
        
        self.conv1=nn.Conv2d(1,out_c1,(kernel_height,kernel_width),stride=self.strd,padding=(2,0))
        self.conv2=nn.Conv2d(1,out_c2,(kernel_height,out_c1*k1_max_pool),stride=self.strd,padding=(2,0))
        self.fc1=nn.Linear(out_c2*k2_max_pool,classes)
        
    def init_embedding(self,use_pretrained=False,pretrained=None,num_embeddings=1000, embedding_dim=10):
        if(use_pretrained):
            self.embed=nn.Embedding.from_pretrained(pretrained,freeze='False')
        else:
            self.embed=nn.Embedding(num_embeddings,embedding_dim)    
    
    def kmax_pooling(x, dim, k):
        index = x.topk(k, dim = dim)[1].sort(dim = dim)[0]
        return x.gather(dim, index)
    
    def forward(self,x):
        #(batch,sen_len,num_word,embed_dim)
        x=self.embed(x)
        #merge sentence and batch dimentions.................        
        #(batch*sen_len,num_word,embed_dim)
        x1=x.view(x.shape[0]*x.shape[1],x.shape[2],x.shape[3])

        #(batch*sen_len,1,num_word,embed_dim)
        x1=x1.unsqueeze(1)                   

        #(batch*sen_len,out_c1,num_word-kernel_h+1+padding*2,1)
        x1=F.relu(self.conv1(x1))

        #(batch*sen_len,out_c1,num_word-kernel_h+1+padding*2)
        x1=x1.view(x1.shape[0],x1.shape[1],x1.shape[2])

        #(batch*sen_len,out_c1,k1_max_pool)
        x1= x1.topk(self.k1_max_pool, dim = 2,sorted=False)[0]

        #separate sentence and batch dimentions.................
        #(batch,sen_len,out_c1*k1_max_pool)
        x1=x1.view(x.shape[0],x.shape[1],x1.shape[1]*x1.shape[2])
        
        #(batch,1,sen_len,out_c1*k1_max_pool)
        x2=x1.unsqueeze(1)        

        #conv2 on senetnces ...
        #(batch, out_c2, out_c1*k1_max_pool-kernel_h+1, 1)
        x2=F.relu(self.conv2(x2))

        #(batch, out_c2, out_c1*k1_max_pool-kernel_h+1)
        x2=x2.view(x2.shape[0],x2.shape[1],x2.shape[2])
        
        #(batch, out_c2,k2_max_pool)
        x2= x2.topk(self.k2_max_pool, dim = 2,sorted=False)[0]
    
         #(batch, out_c2*k2_max_pool)
        x2=x2.view(x2.shape[0],x2.shape[1]*x2.shape[2])

        #(batch,classes)
        op=self.fc1(x2)
        return op

class HAN(nn.Module):
    def __init__(self,input_dim,hidden_dim):
        super(HAN,self).__init__()
        self.hidden_fsize=hidden_dim
        self.input_fsize=input_dim
        self.w_gru=nn.GRU(self.input_fsize,self.hidden_fsize,1,bidirectional=True,bias=False)
        self.w_fc=nn.Linear(self.hidden_fsize*2,self.hidden_fsize*2)
        self.w_context=nn.Parameter(torch.randn(self.hidden_fsize*2))
        self.s_gru=nn.GRU(self.hidden_fsize*2,self.hidden_fsize,1,bidirectional=True,bias=False)
        self.s_fc=nn.Linear(self.hidden_fsize*2,self.hidden_fsize*2)
        self.s_context=nn.Parameter(torch.randn(self.hidden_fsize*2))
        self.fc=nn.Linear(self.hidden_fsize*2,1)
        self.sigmoid=nn.Sigmoid()
    def init_wgru(self,gru,seqlen):
        optim.Adam(gru.parameters())
        i=0
        while(True):
            input = torch.zeros(seqlen,1000,gru.input_size,device=device)
            h0 = torch.zeros(2, 1000,gru.hidden_size,device=device)
            output, hn = gru(input, h0)
#             op_zero=torch.zeros(output.shape,device=device)
            loss=torch.sum(torch.abs(output))
            loss.backward()
            optimizer.step()
            if(i%100==0):
                print(loss.item())
            i+=1
            if(loss.item()<0.00001):
                break
    def init_embedding(self,pretrained):
        self.embed=nn.Embedding.from_pretrained(pretrained,freeze='False')
    def forward1(self,XD,S):#X.shape==Batch_size*num_sntnc*num_words
        #iterate over sentences one by one
        for i in range(XD.shape[1]):
            X1=XD[:,i,:]                      #ith sentence
            X=self.embed(X1)                 #embeding of sentence overBatch o/p X=batch_size*num_word*embed_dim
            X=torch.transpose(X,0,1)        #X=num_word*batch_size*embed_dim
            self.w_gru.flatten_parameters() #Reduce memory uses to explode
            wh,wfh=self.w_gru(X,torch.zeros(2,X.shape[1],X.shape[2]).to(device))#wh=num_word*batch_size*embed_dim
            wh=torch.transpose(wh,0,1)      #wh=(Batch_size*num_sntnc)*num_words*embed_dim
            alpha=torch.tanh(self.w_fc(wh)) # aplpha=(Batch_size*num_sntnc)*num_word*embed_dim importance vector per sentence
            w_attention=torch.matmul(alpha,self.w_context)#w_attention= (Batch_size*num_sntnc)*num_word
#             w_attention=torch.exp(w_attention)
#             total_attention=(1/torch.sum(w_attention,1))
#             w_attention=w_attention*total_attention.unsqueeze(1).expand_as(w_attention)
            w_attention=F.softmax(w_attention,dim=1)
            sntnci=torch.bmm(w_attention.unsqueeze(1),alpha)
            S[:,i,:]=sntnci.squeeze(1)
#             print('wh',wh.shape)
#             print('alpha',alpha.shape)
#             print('sntnci',sntnci.shape)
#             print('total_attention',total_attention.shape)
#             print('w_attention',w_attention.shape)
#             print('w_atention_sum',torch.sum(w_attention,1))

        S=torch.transpose(S,0,1)
        self.s_gru.flatten_parameters()
        sh,sfh=self.s_gru(S,torch.zeros(2,S.shape[1],self.hidden_fsize).to(device))
        sh=torch.transpose(sh,0,1)
        salpha=torch.tanh(self.s_fc(sh))
        s_attention=torch.matmul(salpha,self.s_context)
#         s_attention=torch.exp(s_attention)
#         total_sattention=(1/torch.sum(s_attention,1))
#         s_attention=s_attention*total_sattention.unsqueeze(1).expand_as(s_attention)
        s_attention=F.softmax(s_attention,dim=1)
        review=torch.bmm(s_attention.unsqueeze(1),salpha)
        op=self.fc(review.squeeze(1))
#         print('sh',sh.shape)
#         print('salpha',salpha.shape)
#         print('s_attention',s_attention.shape)
#         print('revivew',review.shape)
#         print('op',op.shape)
        return op,w_attention,s_attention

    def forward(self,XD):#X.shape==Batch_size*num_sntnc*num_words
            #iterate over sentences one by one
            X=XD.view(XD.shape[0]*XD.shape[1],XD.shape[2])
            X=self.embed(X)                 #embeding of sentence overBatch o/p X=batch_size*num_word*embed_dim
            X=torch.transpose(X,0,1)        #X=num_word*batch_size*embed_dim
#             self.w_gru.flatten_parameters() #Reduce memory uses to explode
            wh,wfh=self.w_gru(X,torch.zeros(2,X.shape[1],self.hidden_fsize).to(device))#wh=num_word*batch_size*embed_dim
            wh=torch.transpose(wh,0,1)      #wh=Batch_size*num_sntnc*num_words
            alpha=torch.tanh(self.w_fc(wh)) # aplpha=Batch_size*num_sntnc*embed_dim importance vector per sentence
            w_attention=torch.matmul(alpha,self.w_context)#w_attention= Batch_size*num_sntnc
    #             w_attention=torch.exp(w_attention)
    #             total_attention=(1/torch.sum(w_attention,1))
    #             w_attention=w_attention*total_attention.unsqueeze(1).expand_as(w_attention)
            w_attention=F.softmax(w_attention,dim=1)
            S=torch.bmm(w_attention.unsqueeze(1),alpha)
            S=S.squeeze(1)
            S=S.view(XD.shape[0],XD.shape[1],S.shape[1])
    #             print('wh',wh.shape)
    #             print('alpha',alpha.shape)
    #             print('sntnci',sntnci.shape)
    #             print('total_attention',total_attention.shape)
    #             print('w_attention',w_attention.shape)
    #             print('w_atention_sum',torch.sum(w_attention,1))

            S=torch.transpose(S,0,1)
#             self.s_gru.flatten_parameters()
            sh,sfh=self.s_gru(S,torch.zeros(2,S.shape[1],self.hidden_fsize).to(device))
            sh=torch.transpose(sh,0,1)
            salpha=torch.tanh(self.s_fc(sh))
            s_attention=torch.matmul(salpha,self.s_context)
    #         s_attention=torch.exp(s_attention)
    #         total_sattention=(1/torch.sum(s_attention,1))
    #         s_attention=s_attention*total_sattention.unsqueeze(1).expand_as(s_attention)
            s_attention=F.softmax(s_attention,dim=1)
            review=torch.bmm(s_attention.unsqueeze(1),salpha)
            op=self.fc(review.squeeze(1))
    #         print('sh',sh.shape)
    #         print('salpha',salpha.shape)
    #         print('s_attention',s_attention.shape)
    #         print('revivew',review.shape)
    #         print('op',op.shape)

            return op,w_attention,s_attention

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--train', action='store_true')
    parser.add_argument('--test', action='store_true')
#     device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    print("in main")
    args = parser.parse_args()
    #code
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    filename='glove.6B.50d.txt.word2vec'
    model=KeyedVectors.load_word2vec_format(filename,binary=False)

    tau=1
    current=0
    mb_size=500
    total_doc=25000
    channel=1
    loss_ls=[]
    valloss_ls=[]
    train_acc=0
    embedlen=50
    num_sntnc=20
    num_word=50
    # imdb_buffer=imdb_HAN_dataset('data/imdb-train.csv',num_sntnc,num_word,model,embedlen)
    # imdb_buffer.init_vocab_parameter()

    """critical"""
    #Be Remeber to read imdb_buffer newley if make changes in the imdb_HAN_dataset class
    imdb_buffer1=torch.load('imdb_buffer_slen_20_wlen_50_new_preprocessed')

    # torch.save(imdb_buffer,'imdb_buffer_slen_20_wlen_50_new_preprocessed')

    # # indb_HAN_dataset Varifier Code
    imdb=imdb_HAN_dataset('data/imdb-train.csv',num_sntnc,num_word,model,embedlen)
    imdb.init_vocab_parameter_with_dataset(imdb_buffer1)
    indices=list(range(len(imdb)))#list(range(len(imdb)))
    random.shuffle(indices)
    # train_sampler = SequentialSampler(indices[0:mb_size])
    # val_sampler = SequentialSampler(indices[20000:250000])
    train_sampler = SubsetRandomSampler(indices[0:20000])
    val_sampler = SubsetRandomSampler(indices[20000:25000])
    train_loader=DataLoader(imdb,batch_size=mb_size,num_workers=16,sampler=train_sampler)
    valid_loader=DataLoader(imdb,batch_size=mb_size,num_workers=16,sampler=val_sampler)

    han=HCNN(6,15,4,2,50,5,50,1)
    han.init_embedding(use_pretrained=True,pretrained=imdb.vocabmat.t())

    # if torch.cuda.device_count() > 1:
    #     han=nn.DataParallel(han)
    han.to(device=device)
    criterion=nn.BCEWithLogitsLoss()
    criterion.to(device)
    loss_ls=[]
    optimizer = optim.Adam(han.parameters(),lr=.005) 
    print('complete.......')

    if args.train:
        print('Training...')
        itrn=0
        num_itrn=25
        while(itrn<num_itrn):
            train_acc=0
            total_loss=0
            han.train()
            for numbatch,mb in enumerate(train_loader):
    #             print(type(mb))
    #             print(type(mb['review']))
    #             print(mb['review'].shape)
                optimizer.zero_grad()
                X=mb['review'].to(device=device,dtype=torch.long)
                lbl=mb['rating'].to(device=device,dtype=torch.float)
                ylabel,w_a,s_a=han(X)
    #             loss=-torch.mean(lbl*torch.log(.000001+torch.sigmoid(ylabel).reshape(lbl.shape))+(1-lbl)*torch.log(1.000001-torch.sigmoid(ylabel).reshape(lbl.shape)))
                loss=criterion(ylabel.view(lbl.shape),lbl)
                if(torch.isnan(loss).item()):
                    break
                loss.backward()
                torch.nn.utils.clip_grad_norm_(han.parameters(),90)
                optimizer.step()
                total_loss=total_loss+loss.item()
                ylabel=torch.round(torch.sigmoid(ylabel))
                temp=torch.sum(ylabel.data.to(torch.long).reshape(lbl.shape)==lbl.data.to(torch.long))
                train_acc+=temp.item()
                
            if(torch.isnan(loss).item()):
                    break
            han.eval()
            with torch.no_grad():
                val_acc=0
                val_loss=0
                for (num_val,val_dict) in enumerate(valid_loader):
                    tb=val_dict['review'].to(device,dtype=torch.long) 
                    tlbl=val_dict['rating'].to(device,dtype=torch.float)
                    typred,vwa,vsa=han(tb)
                    temp0=criterion(typred.view(tlbl.shape),tlbl)
                    typred=torch.round(torch.sigmoid(typred))
                    temp1=torch.sum(typred.data.reshape(tlbl.shape)==tlbl.data)
                    val_acc+=temp1.item()
                    val_loss=temp0.item()
                if(itrn%1==0):
                    print("Training loss      :-",round(total_loss,3))
                    print("Training Accuracy  :-",round(float(train_acc)/20000,3))
                    print("Validation loss    :-",round(val_loss,3))
                    print('Validation Accuracy:-',round(float(val_acc)/5000,3))
                    
                loss_ls.append(total_loss)
                valloss_ls.append(val_loss)
            itrn+=1
            total_loss=0   
        state={'model':han.state_dict(),'optimizer':optimizer.state_dict(),'m_itrn':itrn}
        torch.save(state,"current_imdb_han")
    #Experiment Embeding Trainabale pretarined initialized both GRU bais=False 20 setence 50 word
    print('Experiment Embeding Trainabale pretarined initialized both GRU bais=False 20 setence 50 word')
    
    if args.test:
        print('Testing...')
        test_out_file = open('result/imdb-test-hcnn.csv', 'w')
        test_writer = csv.writer(test_out_file, quoting=csv.QUOTE_ALL)
        imdbt= torch.load('imdb_test_20_50')
        indices=range(len(imdbt))
        train_sampler = SequentialSampler(indices)
        train_loader=DataLoader(imdbt,batch_size=1,num_workers=8,sampler=train_sampler)
        df=pd.read_csv('data/imdb-test.csv')
        test_writer.writerow(['review', 'rating'])
        
        trained=torch.load("save_han/hcnn_20_50_train_84")
        han.load_state_dict(trained['model'])
        optimizer.load_state_dict(trained['optimizer'])
        
        han.eval()
        count=0
        for (num_batch,mb_dict) in enumerate(train_loader):
                mb=mb_dict['review'].to(device,dtype=torch.long)
                y=han(mb)
                ypred=torch.round(torch.sigmoid(y))
#                 print(int(ypred.item()))
                for i in range(mb.shape[0]):
                    prediction = ['neg','pos']
                    test_writer.writerow([df.loc[num_batch][0], prediction[int(ypred.item())]])
                
                prediction = ['neg','pos']
                test_writer.writerow([df.loc[num_batch][0], prediction[int(ypred.item())]])
#         trainer.predict()
    print("end")