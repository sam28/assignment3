import os, sys

OKGREEN = '\033[92m'
WARNING = '\033[93m'
FAIL = '\033[91m'
ENDC = '\033[0m'

def check(condition, text, warning=False):
	if condition:
		result = OKGREEN + 'PASS' + ENDC

		if warning:
			return
	elif warning:
		result = WARNING + 'WARN' + ENDC
	else:
		result = FAIL + 'FAIL' + ENDC

	print('[%s] %s' % (result, text))

	if not condition:
		sys.exit(-1)

def get_first_line(filename):
	with open(filename, 'r') as f:
		line = f.readline()

	return line.rstrip()

print('Checking for files...')
check(os.path.isfile('requirements.txt'), 'requirements.txt present')
check(os.path.isfile('readme.md'), 'readme.txt present')
check(os.path.isfile('report.pdf'), 'report.pdf present')
#check(os.path.isfile('imdb-HAN.py'), 'imdb-HAN.py present')
check(os.path.isfile('news-HAN.py'), 'news-HAN.py present')
#check(os.path.isfile('snli.py'), 'snli.py present')
#check(os.path.isfile('twitter.py'), 'twitter.py present')
print('')

accepted_shebangs = ['#!/bin/python2', '#!/bin/python3']

print('Checking shebangs...')
check(get_first_line('imdb-HAN.py') in accepted_shebangs, 'accepted shebang present in imdb-HAN.py')
check(get_first_line('news-HAN.py') in accepted_shebangs, 'accepted shebang present in news-HAN.py')
present_shebangs = set([get_first_line(f) for f in ['imdb-HAN.py','news-HAN.py']])
check(len(present_shebangs) == 1, 'More than one Python used (not going to check virtualenv)', warning=True)
print('')

print('Removing result files...')
os.system('rm result/*')
print('')

print('Attempting to create virtualenv...')
if list(present_shebangs)[0] == '#!/bin/python2':
	os.system('virtualenv --python=python2 /tmp/env-check')
else:
	assert list(present_shebangs)[0] == '#!/bin/python3'
	os.system('virtualenv --python=python3 /tmp/env-check')
print('')

print('Attempting to install packages...')
os.system('/tmp/env-check/bin/pip install -r requirements.txt')
print('')

print('Attempting to run files...')
#print('[imdb-HAN.py]')
#os.system('/tmp/env-check/bin/python imdb-HAN.py --test')
print('[news-HAN.py]')
os.system('/tmp/env-check/bin/python news-HAN.py --test')
print('Checking for result files...')
#check(os.path.isfile('result/imdb-test-han.csv'), 'result/imdb-test-han.csv present')
check(os.path.isfile('result/news-test-han.csv'), 'result/news-test-han.csv present')
print('')

print('Cleaning up...')
print('Removing virtualenv...')
os.system('rm -rf /tmp/env-check')
print('Done!')
